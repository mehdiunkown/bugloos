<?php

namespace App\Models\Scope\Filter;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class AllowedFiltersScope
{
    public function apply(Builder $builder, array $filters): Builder
    {
        foreach ($filters as $key => $filter) {
            $builder->where(Str::snake($key), 'like', "%$filter%");
        }
        return $builder;
    }
}
