<?php

namespace App\Models\Log;

use App\Models\Scope\Filter\AllowedFiltersScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogEntry extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'service_names', 'status_code', 'requested_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'requested_at',
    ];

    public function scopeAllowedFilter(Builder $query, array $filters)
    {
        (new AllowedFiltersScope)->apply($query, $filters);
    }
}
