<?php

namespace App\Presenters\Log;

use App\Presenters\Contracts\Presenter;
use App\Presenters\ImmutableValueObject;
use Carbon\Carbon;

/**
 * @method LogEntryPresenter setServiceNames(string $serviceNames)
 * @method LogEntryPresenter getServiceNames()
 * @method LogEntryPresenter setStatusCode(int $statusCode)
 * @method LogEntryPresenter getStatusCode()
 * @method LogEntryPresenter setRequestedAt(string $requestedAt)
 * @method LogEntryPresenter getRequestedAt()
 */
class LogEntryPresenter implements Presenter
{
    use ImmutableValueObject;

    protected string $serviceNames;
    protected int $statusCode;
    protected string $requestedAt;
}
