<?php

declare(strict_types = 1);

namespace App\Presenters;

trait ImmutableValueObject
{
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->{$name};
        }

        return null;
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->{$name} = $value;
        }

        return $this;
    }

    public function __call($method, $arguments)
    {
        $prefix = substr($method, 0, 3);
        if ($prefix == 'set' || $prefix == 'get') {
            $propertyName = lcfirst(substr($method, 3, strlen($method) - 1));
            return $prefix == 'set' ? $this->__set($propertyName, $arguments[0]) : $this->__get($propertyName);
        }
    }

    public function toArray():array
    {
        return get_object_vars($this);
    }
}
