<?php

namespace App\Http\Controllers;

use App\Service\Search\SearchService;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function getLogDataCount(Request $request)
    {
        $count = SearchService::init(
            $request->only('serviceNames', 'statusCode', 'startDate')
        )->getCountByFilter();

        return response()->json(['count' => $count]);
    }
}
