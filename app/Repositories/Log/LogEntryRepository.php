<?php

namespace App\Repositories\Log;

use App\Models\Log\LogEntry;
use App\Presenters\Log\LogEntryPresenter;
use Illuminate\Support\Arr;

class LogEntryRepository
{
    public function persist(LogEntryPresenter $presenter)
    {
        LogEntry::query()->create([
            'service_names' => $presenter->getServiceNames(),
            'status_code' => $presenter->getStatusCode(),
            'requested_at' => $presenter->getRequestedAt(),
        ]);
    }

    public function getCountByFilter(array $filters)
    {
        return LogEntry::query()->allowedFilter($filters)->count();
    }
}
