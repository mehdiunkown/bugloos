<?php

namespace App\Service\Reader\Contracts;

interface Reader
{
    public function open();
    public function fetch();
}
