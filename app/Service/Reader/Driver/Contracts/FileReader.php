<?php

namespace App\Service\Reader\Driver\Contracts;

interface FileReader
{
    public function setFileName(string $fileName): FileReader;
    public function getFileName(): string;
}
