<?php

namespace App\Service\Reader\Driver;

use App\Service\Reader\Contracts\Reader;
use App\Service\Reader\Driver\Contracts\FileReader as FileReaderContracts;
use JetBrains\PhpStorm\Pure;

class FileReader implements Reader, FileReaderContracts
{
    private string $fileName;
    private $handle;

    #[Pure] public static function init(): FileReader
    {
        return new static();
    }

    /**
     * Or use Storage::driver('local')->get($this->getFileName())
     *
     * @return FileReaderContracts
     */
    public function open(): FileReaderContracts
    {
        $this->handle = fopen($this->getFileName(), 'r');
        return $this;
    }

    /**
     * Or use Storage::driver('local')->get($this->getFileName())
     *
     * @return bool|string
     */
    public function fetch(): bool|string
    {
        return fgets($this->handle);
    }

    public function setFileName(string $fileName): FileReader
    {
        $this->fileName = $fileName;
        return $this;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }
}
