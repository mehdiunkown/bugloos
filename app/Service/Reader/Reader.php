<?php

namespace App\Service\Reader;
use App\Service\Reader\Contracts\Reader as ReaderDriver;
use Illuminate\Support\LazyCollection;
use JetBrains\PhpStorm\Pure;

class Reader
{
    /** @var ReaderDriver */
    protected ReaderDriver $readerDriver;

    public function __construct(ReaderDriver $readerDriver)
    {
        $this->readerDriver = $readerDriver;
    }

    #[Pure] public static function init(ReaderDriver $readerDriver): static
    {
        return new static($readerDriver);
    }

    public function getReaderDriver(): ReaderDriver
    {
        return $this->readerDriver;
    }

    public function read(): LazyCollection
    {
        return LazyCollection::make(function () {
            $this->getReaderDriver()->open();

            while (($line = $this->getReaderDriver()->fetch()) !== false) {
                yield $line;
            }
        });
    }
}
