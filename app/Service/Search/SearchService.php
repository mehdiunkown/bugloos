<?php

namespace App\Service\Search;

use App\Repositories\Log\LogEntryRepository;
use JetBrains\PhpStorm\Pure;

class SearchService
{
    private array $filters = [];

    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    #[Pure] public static function init(array $filters): SearchService
    {
        return new static($filters);
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getCountByFilter()
    {
        return app(LogEntryRepository::class)->getCountByFilter($this->getFilters());
    }
}
