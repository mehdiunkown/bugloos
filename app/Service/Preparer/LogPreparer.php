<?php

namespace App\Service\Preparer;

use App\Presenters\Contracts\Presenter;
use App\Presenters\Log\LogEntryPresenter;
use App\Service\Preparer\Contracts\Preparer;
use Carbon\Carbon;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\Pure;

class LogPreparer implements Preparer
{
    protected string $content;

    public function __construct(string $content)
    {
        $this->content = trim($content);
    }

    #[Pure] public static function init(string $content): LogPreparer
    {
        return new static($content);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    public function toPresenter(): LogEntryPresenter
    {
        // or use accessor and mutator
        $presenter = new LogEntryPresenter;
        return $presenter->setServiceNames($this->getServiceNames())
            ->setStatusCode($this->getStatusCode())
            ->setRequestedAt($this->getRequestedAt());
    }

    private function getServiceNames(): string
    {
        echo $this->content;
        return rtrim(Str::beforeLast($this->content, '-'));
    }

    private function getStatusCode(): int
    {
        $statusCode = substr($this->content, -3);
        return (int) $statusCode;
    }

    private function getRequestedAt(): string
    {
        $dateTime = Str::between($this->content, '[', ']');
        $time = Str::after($dateTime, ':');
        $requestedAt = Str::of(Str::replace('/','-', Str::before($dateTime, ':')))->append(" $time");
        return Carbon::parse($requestedAt)->toDateTimeString();
    }
}
