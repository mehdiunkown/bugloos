<?php

namespace App\Service\Preparer\Contracts;

use App\Presenters\Contracts\Presenter;

interface Preparer
{
    public function toPresenter(): Presenter;
}
