<?php

namespace App\Console\Commands;

use App\Repositories\Log\LogEntryRepository;
use App\Service\Preparer\LogPreparer;
use App\Service\Reader\Driver\FileReader;
use App\Service\Reader\Reader;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ParseLogFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse log file for persist into database';

    protected LogEntryRepository $repository;

    public function __construct(LogEntryRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->info("start at: " . $this->getCurrentDateTime());
        $this->processingLogContent();
        $this->info("end at: " . $this->getCurrentDateTime());
    }

    public function processingLogContent()
    {
        $readerDriver = FileReader::init()->setFileName(storage_path('app/public/logs.txt'));
        $lazyCollection = Reader::init($readerDriver)->read();
        // or use `chunk(5)->map()->each()
        $lazyCollection->each(function (string $line) {
            preg_replace( "/\r|\n/", "", $line);
            $presenter = LogPreparer::init($line)->toPresenter();
            $this->repository->persist($presenter);
        });;
    }

    private function getCurrentDateTime(): string
    {
        return now()->toDateTimeString();
    }
}
