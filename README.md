## If you need to parse the log content, please run the following command to parse and store the data in the database.
### `php artisan parse:log`

## Api for get count of log content based on filter:
### `{baseUrl}/api/logs/count`

## Test steps:
- Http test for `api/logs/count`
- Feature test for `SearchService` and `getCountByFilter` method 
